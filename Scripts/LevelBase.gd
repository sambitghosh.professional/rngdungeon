tool
extends Spatial


export var GroundScene: PackedScene 

export var map_lenght = 10 setget set_map_lenght, get_map_lenght
export var map_width = 10 setget set_map_width, get_map_width

# Called when the node enters the scene tree for the first time.
func _ready():
	generate_map_base()
	
func get_map_lenght():
	return map_lenght

func get_map_width():
	return map_width

func set_map_lenght(var lenght):
	if lenght < 1:
		lenght = 1
	map_lenght = lenght
	generate_map_base()

func set_map_width(var width):
	if width < 1:
		width = 1
	map_width = width
	generate_map_base()
	
func clear_map():
	for node in get_children():
		if typeof(node) == typeof(GroundScene.instance()):
			node.queue_free()

func generate_map_base():
	print ( " Generating map ... ")
	
	clear_map()
	# make map grid
	var startingX = -(map_lenght - 1)
	var startingZ = -(map_width - 1)
	for i in range(0,map_lenght):
		for j in range(0,map_width):
			var ground_peice = GroundScene.instance()
			ground_peice.transform.origin.x = startingX + 2*i;
			ground_peice.transform.origin.z = startingZ + 2*j;
			
			add_child(ground_peice)
	
