extends KinematicBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var health = 100
export var speed = 5
var gravity = 9.8
var velocity = Vector3()
var charecter
var taking_damage =false
var enemies_attaking :Array
var enemies_to_attack : Array


# Called when the node enters the scene tree for the first time.
func _ready():
	charecter = get_node(".")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _process(delta):
	
	var dir = Vector3()
	var is_moving = false
	dir.y = 0
	
	if(Input.is_action_pressed("ui_up")):
		dir.x+=1
		dir.z-=1
		is_moving = true
	if(Input.is_action_pressed("ui_down")):
		dir.x-=1
		dir.z+=1
		is_moving = true
	if(Input.is_action_pressed("ui_right")):
		dir.x+=1
		dir.z+=1
		is_moving = true
	if(Input.is_action_pressed("ui_left")):
		dir.x-=1
		dir.z-=1
		is_moving = true
	
	dir=dir.normalized()
	
	velocity.y = - gravity
	
	velocity.x = dir.x * speed
	velocity.z = dir.z * speed
	move_and_slide(velocity)
	
	if is_moving:
		var angle = atan2(velocity.x,velocity.z)
		
		var char_rot = charecter.get_rotation()
		char_rot.y = angle + 80.2
		charecter.set_rotation(char_rot)
		
	if Input.is_action_just_pressed("ui_accept"):
		damage_enemies()
	
	#take_damage(delta)
		
func take_damage(delta):
	for body in enemies_attaking:
		health -= body.damage_to_player

func _on_PlayerArea_body_entered(body):
	
	if body.is_in_group("enemy"):
		enemies_attaking.append(body)
	pass # Replace with function body.


func _on_PlayerArea_body_exited(body):
	if body.is_in_group("enemy"):
		enemies_attaking.erase(body)
	pass # Replace with function body.

func damage_enemies():
	for body in enemies_to_attack:
		body.health -= 4
		
		var enemy_pos = body.transform.origin
		var dir = charecter.transform.origin.direction_to(enemy_pos)
	
		dir=dir.normalized()
		var velo : Vector3
		velo.x= dir.x * speed *15
		velo.y=0
		velo.z= dir.z * speed *15
		#body.move_and_slide( velocity )
		body.apply_central_impulse(velo)
		
	pass


func _on_SwordAttackArea_body_entered(body):
	if body.is_in_group("enemy"):
		enemies_to_attack.append(body)
	pass # Replace with function body.


func _on_SwordAttackArea_body_exited(body):
	if body.is_in_group("enemy"):
		enemies_to_attack.erase(body)
	pass # Replace with function body.
