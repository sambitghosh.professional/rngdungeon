tool
extends MultiMeshInstance

export var GroundScene: PackedScene 

export var map_lenght = 15 setget set_map_lenght, get_map_lenght
export var map_width = 10 setget set_map_width, get_map_width

export var map_seed = 1234 setget set_seed, get_seed

# Called when the node enters the scene tree for the first time.
func _ready():
	generate_cooridoors()
	
func set_seed(var new_seed):
	map_seed = new_seed
	generate_cooridoors()
	
func get_seed():
	return map_seed	

func get_map_lenght():
	return map_lenght

func get_map_width():
	return map_width

func set_map_lenght(var lenght):
	if lenght < 1:
		lenght = 1
	map_lenght = lenght
	generate_cooridoors()

func set_map_width(var width):
	if width < 1:
		width = 1
	map_width = width
	generate_cooridoors()
	
func clear_map():
	for instance in 1000:
		self.multimesh.get_rid()

func generate_cooridoors():
	print ( " Generating corrodoor ... ")
	
	clear_map()
	seed(map_seed)
	# make map grid
#	var startingX = -(map_lenght - 1)
#	var startingZ = -(map_width - 1)
#	for i in range(0,map_lenght):
#		for j in range(0,map_width):
#			var ground_peice = GroundScene.instance()
#			ground_peice.transform.origin.x = startingX + 2*i;
#			ground_peice.transform.origin.z = startingZ + 2*j;
#
#			add_child(ground_peice)
	var instance_id : int = 0
	var prev_z = 0
#	for x in range (-15,+15):
#		var direction : int = rand_range(0,3)
#		var iterations : int = rand_range(0,10)
#		for j in range (iterations):
#			instance_id +=1
#
	
	self.multimesh.instance_count = 1000

	instance_id  = 0
	clear_map()
	
	for x in range (-30,+30):
		var direction : int = rand_range(0,3)
		var iterations : int = rand_range(2,10)
		#self.multimesh.set_instance_transform(instance_id,Transform(Basis(),Vector3(x*2,0,prev_z)))
		#instance_id += 1
		for j in range (iterations):
			var z = prev_z
			
			self.multimesh.set_instance_transform(instance_id,Transform(Basis(),Vector3(x*2,0,z)))
			if x % 5 == 0:
				if direction == 1:
					z+=2
				else:
					if direction == 0:
						z-=2
			instance_id += 1
			self.multimesh.set_instance_transform(instance_id,Transform(Basis(),Vector3((x)*2,0,z)))
			instance_id += 1
			prev_z = z

