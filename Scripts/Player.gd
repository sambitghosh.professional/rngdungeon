extends KinematicBody

export var bulletscene : PackedScene
export var health = 100
export var speed = 5
var gravity = 9.8
var velocity = Vector3()
var charecter
var taking_damage =false
var enemies_attaking :Array
var enemies_to_attack : Array
var enemies_in_range : Array
var CharacterScene
var CharAnimator : AnimationPlayer
var CharAnimTree : AnimationTree
var attack_cooldown = 0.5
var time_since_attacked = 0
var bulletfireloc

var moving_anim_toggle = "parameters/loopblend/blend_amount"
var use_anim_trigger = "parameters/useanim/active"


# Called when the node enters the scene tree for the first time.
func _ready():
	charecter = get_node(".")
	CharacterScene = get_node("charecter6")
	CharAnimator = get_node("charecter6/AnimationPlayer")
	CharAnimTree = get_node("charecter6/AnimationTree")
	bulletfireloc = get_node("BulletFireLoc")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _process(delta):
	
	var dir = Vector3()
	var is_moving = false
	dir.y = 0
	
	if(Input.is_action_just_pressed("use_ranged")):
		damage_ranged()
	
	if(Input.is_action_pressed("ui_up")):
		dir.x+=1
		dir.z-=1
		is_moving = true
	if(Input.is_action_pressed("ui_down")):
		dir.x-=1
		dir.z+=1
		is_moving = true
	if(Input.is_action_pressed("ui_right")):
		dir.x+=1
		dir.z+=1
		is_moving = true
	if(Input.is_action_pressed("ui_left")):
		dir.x-=1
		dir.z-=1
		is_moving = true
	
	dir=dir.normalized()
	
	velocity.y = - gravity
	
	velocity.x = dir.x * speed
	velocity.z = dir.z * speed
	move_and_slide(velocity)
	
	time_since_attacked += delta
	
	if is_moving:
		var angle = atan2(velocity.x,velocity.z)
		
		var char_rot = charecter.get_rotation()
		char_rot.y = angle + 80.2
		charecter.set_rotation(char_rot)
		CharAnimTree.set(moving_anim_toggle,1)
	else:
		CharAnimTree.set(moving_anim_toggle,0)
		
		#CharAnimator.play("walkfeet")
#		if !CharAnimator.is_playing():
#			CharAnimator.play("walkfeet")
			
	if Input.is_action_just_pressed("use_ranged"):
		damage_ranged()
		
	if Input.is_action_just_pressed("ui_accept"):
		
		damage_enemies()
	else:
		if CharAnimator.assigned_animation == "walkfeet" and !is_moving:
			CharAnimator.stop()
	
	#take_damage(delta)
		
func take_damage(delta):
	for body in enemies_attaking:
		health -= body.damage_to_player

func damage_enemies():
	if time_since_attacked >= attack_cooldown:
		CharAnimTree.set(use_anim_trigger,false)
		time_since_attacked = 0
		CharAnimTree.set(use_anim_trigger,true)
		for body in enemies_to_attack:
			body.health -= 4
			body.is_just_attacked = true

	pass

func damage_ranged():
	var mindist = 100
	var closest_enemy
	for body in enemies_in_range :
		var dist = self.transform.origin.distance_to(body.transform.origin)
		if mindist >= dist :
			mindist = dist
			closest_enemy = body
	print ("Bodies in range: ", enemies_in_range.size())
	print("mindist " , mindist)
	if enemies_in_range.size() > 0 :
		print("Firing Ranged")
		fire_ranged_at(closest_enemy)
	
	pass

func fire_ranged_at(body):
	var bullet_inst = bulletscene.instance()
	var target_loc = Vector3(body.transform.origin.x,1.5,body.transform.origin.z)
	bullet_inst.Target_loc = target_loc
	
	var loc = Vector3(bulletfireloc.transform.origin.x, 1.5,bulletfireloc.transform.origin.z)
	bullet_inst.transform.origin = loc
	self.get_parent().get_parent().add_child(bullet_inst)
	
	
	pass

func _on_SwordAttackArea_body_entered(body):
	if body.is_in_group("enemy"):
		enemies_to_attack.append(body)
	pass # Replace with function body.


func _on_SwordAttackArea_body_exited(body):
	if body.is_in_group("enemy"):
		enemies_to_attack.erase(body)
	pass # Replace with function body.


func _on_RangedWeaponArea_body_entered(body):
	if body.is_in_group("enemy"):
		enemies_in_range.append(body)
	pass # Replace with function body.


func _on_RangedWeaponArea_body_exited(body):
	if body.is_in_group("enemy"):
		enemies_in_range.erase(body)
	pass # Replace with function body.
