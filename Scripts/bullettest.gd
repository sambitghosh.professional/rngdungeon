extends KinematicBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var BulletDamage : int
var Target_loc : Vector3
var dir :Vector3
var velocity : Vector3
var speed = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	print("bullet made,moving to ", Target_loc.x,",",Target_loc.z)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	dir = self.transform.origin.direction_to(Target_loc)
	dir = dir.normalized()
	
	velocity.x = dir.x * speed
	velocity.y = 0
	velocity.z = dir.z * speed
	
	move_and_slide(velocity)


func _on_BulletArea_body_entered(body):
	if body.is_in_group("enemy"):
		body.health -= 5
		body.is_just_attacked = true
		self.queue_free()
	elif !body.is_in_group("player"):
		self.queue_free()
	pass # Replace with function body.
