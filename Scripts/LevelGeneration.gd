
extends Spatial

var borders = Rect2(-1, -1, 128, 128)
var levelmap = []
var cam

export var PlayerScene : PackedScene
export var EnemyScene : PackedScene
export var mapseed = 12346 setget set_mapseed, get_mapseed

var Player_instance
var Enemy_instance_array : Array
var Lantern
var healthbar

func running() -> bool:
	return !Engine.editor_hint

func set_mapseed(var newseed):
	if running():
		yield(self,"ready")
	mapseed = newseed
	seed(mapseed)
	if !running():
		generate_level()

func get_mapseed():
	return mapseed

func _ready():
	seed(mapseed)
	cam = get_node("Camera")
	Lantern = get_node("SpotLight")
	healthbar = get_node("Control/ProgressBar")
	generate_level()
	
func generate_level():
	$GridMap.clear()
	var walker = Walker.new(Vector2(0,0), borders)
	var map = walker.walk(1000)
	levelmap = map
	walker.queue_free()
	for location in map:
		$GridMap.set_cell_item(location.x,0,location.y,2)
	
	var wall_map = get_walls_location()
	
	#place walls
	for location in wall_map:
		
		$GridMap.set_cell_item(location.x,0,location.y,3)
		$GridMap.set_cell_item(location.x,1,location.y,3)
		
	Player_instance = PlayerScene.instance()
	Player_instance.transform.origin.x = 2
	Player_instance.transform.origin.y = 0.5
	Player_instance.transform.origin.z = 2
	add_child(Player_instance)
	
	Spawn_Enemies()
	
func Spawn_Enemies():
	var spawnablecells=[]
	for cell in levelmap:
		var location = Vector2(cell.x,cell.y)
		var Player_loc = Vector2(Player_instance.get_child(0).transform.origin.x, Player_instance.get_child(0).transform.origin.z)
		if location.distance_to(Player_loc) > 10:
			if(levelmap.find(Vector2(location.x + 1,location.y)) >= 1):
				if(levelmap.find(Vector2(location.x - 1,location.y)) >= 1):
					if(levelmap.find(Vector2(location.x,location.y + 1)) >= 1):
						if(levelmap.find(Vector2(location.x,location.y - 1)) >= 1):
							spawnablecells.append(location)
	
	spawnablecells.shuffle();
	for i in range (20):
		var enemy_inst = EnemyScene.instance()
		enemy_inst.transform.origin.x = spawnablecells[i].x * 2
		enemy_inst.transform.origin.y = 0.2
		enemy_inst.transform.origin.z = spawnablecells[i].y * 2
		add_child(enemy_inst)
		Enemy_instance_array.append(enemy_inst)
	pass

func get_walls_location():
	var wall_map = []
	for location in levelmap:
		if(levelmap.find(Vector2(location.x + 1,location.y)) == -1):
			wall_map.append(Vector2(location.x + 1,location.y))
		
		if(levelmap.find(Vector2(location.x - 1,location.y)) == -1):
			wall_map.append(Vector2(location.x - 1,location.y))
			
		if(levelmap.find(Vector2(location.x,location.y + 1)) == -1):
			wall_map.append(Vector2(location.x,location.y + 1))
		
		if(levelmap.find(Vector2(location.x,location.y - 1)) == -1):
			wall_map.append(Vector2(location.x,location.y - 1))
		
		if(levelmap.find(Vector2(location.x + 1,location.y + 1)) == -1):
			wall_map.append(Vector2(location.x + 1,location.y + 1))
		
		if(levelmap.find(Vector2(location.x - 1,location.y + 1)) == -1):
			wall_map.append(Vector2(location.x - 1,location.y + 1))
			
		if(levelmap.find(Vector2(location.x + 1,location.y - 1)) == -1):
			wall_map.append(Vector2(location.x + 1,location.y - 1))
		
		if(levelmap.find(Vector2(location.x - 1,location.y - 1)) == -1):
			wall_map.append(Vector2(location.x - 1,location.y - 1))
	
	return wall_map

func check_enemies():
	for enemy in Enemy_instance_array:
		if enemy.is_alive != true :
			Enemy_instance_array.erase(enemy)
			enemy.queue_free()


func _process(_delta):
	var inst = Player_instance.get_child(0)
	cam.transform.origin.x = inst.transform.origin.x - 3
	cam.transform.origin.z = inst.transform.origin.z + 7
	
	Lantern.transform.origin.x = inst.transform.origin.x + 2.0
	Lantern.transform.origin.z = inst.transform.origin.z + 2.0
	Lantern.transform.origin.y = 5
	
	healthbar.value = inst.health
	check_enemies()
