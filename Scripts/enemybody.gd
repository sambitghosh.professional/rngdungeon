extends RigidBody


export var speed = 2
export var damage_to_player = 2
export var health = 10
var is_alive = true
var gravity = 9.8
var velocity = Vector3()
var enemy_self
var time_since_turn = 0
var time_since_damage = 0
var damage_cooldown = 0.5
var dir : Vector2
const Direction = [Vector2.UP,Vector2.DOWN,Vector2.LEFT,Vector2.RIGHT]
var range_detector : CollisionShape
var attacking = false 
var player_in_range = false
var player_body : KinematicBody
var body_to_attack : KinematicBody
var CharAnim : AnimationPlayer
var is_dead = false
var is_just_attacked = false


# Called when the node enters the scene tree for the first time.
func _ready():
	enemy_self = get_node(".")
	
	pass # Replace with function body.


func idle_roaming(delta):
	
	if time_since_turn > 1 :
		time_since_turn = 0
		var dir_array = Direction.duplicate()
		dir_array.shuffle()
		
		for i in range(rand_range(0,2)):
			dir.x += dir_array[i].x
			dir.y += dir_array[i].y
		
	dir = dir.normalized()
	velocity.x = dir.x * speed
	velocity.z = dir.y * speed
	velocity.y = - gravity
	#move_and_slide( velocity )
	self.velocity = velocity
	var angle = atan2(velocity.x,velocity.z)
	var rot = self.get_rotation() 
	rot.y = angle - 80.2
	self.set_rotation(rot)
	 
	time_since_turn += delta

func attack_player():
	var player_pos = player_body.transform.origin
	player_pos.x = player_pos.x +2
	player_pos.z = player_pos.z +2
	var self_pos = self.transform.origin
	var dir = self_pos.direction_to(player_pos)
	
	dir=dir.normalized()
	velocity.x= dir.x * speed
	velocity.z= dir.z * speed
	#move_and_slide( velocity )
	self.velocity = velocity
	
	var angle = atan2(velocity.x,velocity.z)
	var rot = self.get_rotation() 
	rot.y = angle - 80
	self.set_rotation(rot)
	
	pass

func get_stunned():
	var player_pos = player_body.transform.origin
	player_pos.x = player_pos.x +2
	player_pos.z = player_pos.z +2
	var self_pos = self.transform.origin
	var dir = player_pos.direction_to(self_pos)
	
	dir=dir.normalized()
	velocity.x= dir.x * speed
	velocity.z= dir.z * speed
	#move_and_slide( velocity )
	self.velocity = velocity
	
	pass

func do_damage(delta):
	if time_since_damage >= damage_cooldown:
		time_since_damage = 0
		body_to_attack.health -= damage_to_player

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _process(delta):
	velocity.y = - gravity
	if player_in_range:
		attack_player()
	else:
		idle_roaming(delta)
	if is_just_attacked:
		get_stunned()
	else:
		if attacking:
			do_damage(delta)
	time_since_damage += delta
	if health<=0 and is_dead == false :
#		print("Dying")
#		CharAnim.play("death")
#		is_dead=true
#	if !CharAnim.is_playing() and is_dead == true:
		is_alive=false
	

func _on_AttackArea_body_entered(body):
	if body.is_in_group("player"):
		attacking= true
		body_to_attack = body
	pass # Replace with function body.


func _on_AttackArea_body_exited(body):
	if body.is_in_group("player"):
		attacking= false
	pass # Replace with function body.


func _on_DetectionArea_body_entered(body):
	if body.is_in_group("player") :
		player_in_range = true
		player_body = body
	pass # Replace with function body.


func _on_DetectionArea_body_exited(body):
	if body.is_in_group("player") :
		player_in_range = false
	pass # Replace with function body.
