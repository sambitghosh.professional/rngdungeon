tool
extends Spatial

var GroundBase = get_parent().find_node("GroundBase")

export var Pillar : PackedScene

var map_lenght = GroundBase.get_map_lenght() setget set_map_lenght, get_map_lenght
var map_width = GroundBase.get_map_width() setget set_map_width, get_map_width

export (float,0,1,0.05) var density = 0 setget set_density, get_density
export (int) var mapseed = 1 setget set_seed, get_seed

var map_coords_array : Array = []

class Coord:
	var x : int
	var z : int
	
	func _init(xs,zs):
		x=xs
		z=zs
		

# Called when the node enters the scene tree for the first time.
func _ready():
	generate_obstacles()
	
func clear_obstacles():
	for node in get_children():
		node.queue_free()
	
func set_seed(var new_seed):
	mapseed = new_seed
	generate_obstacles()
	
func get_seed():
	return mapseed

func set_density(var new_density):
	density = new_density
	generate_obstacles()
	
	
func get_density():
	return density
	
func get_map_lenght():
	return map_lenght

func get_map_width():
	return map_width

func set_map_lenght(var lenght):
	if lenght < 1:
		lenght = 1
	map_lenght = lenght
	
func set_map_width(var width):
	if width < 1:
		width = 1
	map_width = width
	
func fill_coord_array():
	map_coords_array = []
	for i in range(0,map_lenght):
		for j in range(0,map_width):
			map_coords_array.append(Coord.new(i,j))
			
func make_pillar_at(x,z):
	var position = Vector3(x*2,0,z*2)
	position += Vector3(-map_lenght+1,0,-map_width+1)
	var new_pillar = Pillar.instance()
	new_pillar.transform.origin = position
	add_child(new_pillar)

func generate_pillars():
	clear_obstacles()
	fill_coord_array()
	seed(mapseed)
	map_coords_array.shuffle()
	
	var num_pillars : int = (map_coords_array.size() * density)
	var coords = []
	for i in range(0,num_pillars-1):
		coords.append(map_coords_array[i])
		
	for coord in coords:
		make_pillar_at(coord.x,coord.z)

func generate_obstacles():
	map_lenght = GroundBase.get_map_lenght()
	map_width = GroundBase.get_map_width() 
	generate_pillars()
